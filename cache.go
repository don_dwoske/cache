// cache is a package to manage a cache of []byte entities.
// You will need to write a function with a particular
// signature (cache.GetMore) which will be called when
// the cache is empty and needs to be replenished.  After
// setting up the cache, the caller needs only to call
// cache.Fetch(x) to retrieve some items, when the cache
// is empty, it will call your GetMore function automatically
// to refill itself.  Any errors returned from your GetMore
// function are passed through in the case of a problem.
// The package currently uses BoltDB to store the items
// where a timestamp is the key and the []byte slice
// is the value.
package cache

import (
	"encoding/binary"
	// "log"
	"fmt"
	"os"
	"time"

	"github.com/boltdb/bolt"
)

// Cache will add a cache capability on a stream of
// []byte items
type Cache interface {
	Fetch(int) ([][]byte, error)
}

// GetMore is called when the cache is empty and needs
// to be repopulated, it should return an array of size
// count.. the cache just stores an array of bytes
type GetMore func(repopulateCount int) ([][]byte, error)

// Shared BoltDB instance.  All the caches will use this
// with buckets matching the name of the cache
var db *bolt.DB

// BoltCache implements Cache using BoltDB
type CacheImpl struct {
	name            string
	repopulateCount int
	getMore         GetMore
}

func initDb() error {
	if db == nil {
		var err error
		f := fmt.Sprintf("%s/cache.db", os.Getenv("HOME"))
		db, err = bolt.Open(f, 0644, nil)
		if err != nil {
			return err
		}
	}
	return nil
}

// NewCache returns a new cache backed by BoltDB
func NewCache(name string, repopulateCount int, getMore GetMore) (Cache, error) {

	if db == nil {
		err := initDb()
		if err != nil {
			return nil, err
		}
	}

	db.Update(func(tx *bolt.Tx) error {
		_, err1 := tx.CreateBucketIfNotExists([]byte(name))
		if err1 != nil {
			return err1
		}
		return nil
	})

	return CacheImpl{name, repopulateCount, getMore}, nil
}

// Fetch returns count items from the cache, if there are not
// enough the cache is repopulated using the GetMore
// function
func (cache CacheImpl) Fetch(count int) ([][]byte, error) {

	// log.Printf("cache:Fetch requesting %d\n", count)
	bs := make([][]byte, 0)

	for len(bs) < count {
		more, err := cache.fetchit(count - len(bs))

		bs = append(bs, more...)
		if err != nil {
			return nil, err
		}

		if len(bs) < count {
			err := cache.doGetMore()
			if err != nil {
				return nil, err
			}
		}
	}
	return bs, nil
}

// uint64ToBytes Converts a uint to a byte slice
func uint64ToBytes(u uint64) []byte {
	buf := make([]byte, 8)
	binary.BigEndian.PutUint64(buf, u)
	return buf
}

func (cache CacheImpl) doGetMore() error {
	// log.Println("cacheImpl:getMore need to repopulate cache")
	err := db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(cache.name))
		more, err := cache.getMore(cache.repopulateCount)
		if err != nil {
			return err
		}

		for _, item := range more {
			t := uint64(time.Now().UnixNano())
			key := uint64ToBytes(t)
			bucket.Put(key, item)
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

func (cache CacheImpl) fetchit(count int) ([][]byte, error) {

	// log.Printf("cache.fetchit Getting %d items from cache.\n", count)
	bs := make([][]byte, 0)
	err := db.Update(func(tx *bolt.Tx) error {

		bucket := tx.Bucket([]byte(cache.name))
		cursor := bucket.Cursor()

		i := 0

		for k, v := cursor.First(); k != nil; k, v = cursor.Next() {
			t := make([]byte, len(v))
			copy(t, v)
			bs = append(bs, t)
			cursor.Delete()
			i = i + 1
			if i == count {
				break
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return bs, nil
}
